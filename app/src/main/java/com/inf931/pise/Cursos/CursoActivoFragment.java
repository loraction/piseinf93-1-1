package com.inf931.pise.Cursos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.inf931.pise.R;
import com.inf931.pise.VolleyRP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class CursoActivoFragment extends Fragment {

    TextView txtNombre;
    TextView txtImpartida;
    TextView txtFecha;
    RadioButton rdbSi;
    RadioButton rdbNo;
    Button btnConfirmar;

    String URL = "http://travelopolis.ddns.net/pise/Cursos.GETCURSO.php?usuario=";
    String URLDELETE = "http://travelopolis.ddns.net/pise/Cursos_DELETE.php";
    String IDCURSO;
    String IDALUMNO;

    private VolleyRP volley;
    private RequestQueue mRequest;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_curso_activo, container, false);

        SharedPreferences prefs = getActivity().getBaseContext().getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);
        String idusuario = prefs.getString("idusuario","No Disponible");
        IDALUMNO = idusuario;

        volley = VolleyRP.getInstance(getContext());
        mRequest = volley.getRequestQueue();

        txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        txtImpartida = (TextView) view.findViewById(R.id.txtImpartida);
        txtFecha = (TextView) view.findViewById(R.id.txtFecha);
        rdbSi = (RadioButton) view.findViewById(R.id.AsistirSi);
        rdbNo = (RadioButton) view.findViewById(R.id.Asistirno);

        btnConfirmar = (Button) view.findViewById(R.id.btnActualizar);
        solicitudJSON(URL+idusuario);

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pivote = 0;
                if(rdbSi.isChecked()){
                    pivote = 1;
                }
                if(rdbNo.isChecked()){
                    pivote = 2;
                }

                if(pivote==1){
                    Fact(2);
                }
                else{
                    eleccion();
                }
            }
        });
        return view;
    }


    public void solicitudJSON(String URL){
        JsonObjectRequest solicitud = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject datos) {
                verificarCurso(datos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Ocurrio un Error"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,getContext(),volley);
    }

    public void verificarCurso(JSONObject datos){
        //Controllar el JSON
        //Toast.makeText(this,"Los Datos son:"+datos.toString(),Toast.LENGTH_SHORT).show();
        try{
            String estado = datos.getString("resultado");
            if(estado.equals("CC")){
                JSONObject JsonDatos = new JSONObject(datos.getString("datos"));
                String id= JsonDatos.getString("id");
                IDCURSO = id;
                String nombre= JsonDatos.getString("nombre");
                String imparte = JsonDatos.getString("imparte");
                String fecha = JsonDatos.getString("fecha");
                String hora = JsonDatos.getString("hora");

                txtNombre.setText(nombre);
                txtFecha.setText(fecha+" / "+hora);
                txtImpartida.setText(imparte);
                rdbSi.setChecked(true);
            }else{
                txtNombre.setText("Sin Nombre");
                txtFecha.setText("Sin Fecha");
                txtImpartida.setText("Sin Ponente");
                btnConfirmar.setEnabled(false);
                Fact(1);
            }

        }catch (JSONException e){}
    }


    public void ejecutarEliminar(){
        Map<String, String> parametros = new HashMap<>();
        parametros.put("usuario",IDALUMNO);
        parametros.put("clase",IDCURSO);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, URLDELETE, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(getActivity(), response.getString("resultado"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Error en: "+ error, Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(solicitud);

    }
    public void Fact(int seleccion){
        //se prepara la alerta creando nueva instancia
        AlertDialog.Builder alertbox = new AlertDialog.Builder(getContext());
        //seleccionamos la cadena a mostrar
        if(seleccion == 1){
            alertbox.setMessage("Sin Curso Activo");
        }
        if(seleccion == 2){
            alertbox.setMessage("Sin Cambios de Confirmacion");
        }
        alertbox.show();
    }

    public void eleccion(){
        AlertDialog.Builder alertbox = new AlertDialog.Builder(getContext());
        alertbox.setMessage("¿Deseas Eliminar Tu Confirmacion?");
        alertbox.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            //Funcion llamada cuando se pulsa el boton Si
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            public void onClick(DialogInterface arg0, int arg1) {
                ejecutarEliminar();
                Toast.makeText(getActivity(),"Cancelando Confirmacion", Toast.LENGTH_SHORT).show();
                getActivity().finishAndRemoveTask();
            }
        });
        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            //Funcion llamada cuando se pulsa el boton No
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(getActivity(),"Abortado", Toast.LENGTH_SHORT).show();
            }
        });
        alertbox.show();
    }

}
