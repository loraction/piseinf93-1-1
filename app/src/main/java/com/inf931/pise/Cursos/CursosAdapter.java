package com.inf931.pise.Cursos;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.inf931.pise.R;
import com.inf931.pise.VolleyRP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CursosAdapter extends RecyclerView.Adapter<CursosAdapter.HolderVacante> {

    String IDUSUARIO;
    String IDCURSO;

    private VolleyRP volley;
    private RequestQueue mRequest;

    private List<CursoConstructor> atributosList;
    private Context context;
    String URL = "http://travelopolis.ddns.net/pise/Cursos_AGRECLASE.php";

    public CursosAdapter(List<CursoConstructor> atributosList, Context context){
        this.atributosList = atributosList;
        this.context = context;

        SharedPreferences prefs = context.getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);
        String idusuario = prefs.getString("idusuario","No Disponible");
        IDUSUARIO = idusuario;

        volley = VolleyRP.getInstance(context);
        mRequest = volley.getRequestQueue();

    }

    @Override
    public CursosAdapter.HolderVacante onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_curso,parent,false);
        return new CursosAdapter.HolderVacante(v);
    }

    @Override
    public void onBindViewHolder(final CursosAdapter.HolderVacante holder, final int position) {
        String id = atributosList.get(position).getId();
        String nombre = atributosList.get(position).getNombre();
        String universidad = atributosList.get(position).getUniversidad();
        String centro = atributosList.get(position).getCentro();

        holder.nombre.setText(nombre);
        holder.universidad.setText(universidad);
        holder.centro.setText(centro);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ejecutarEliminar(atributosList.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }
    static class HolderVacante extends RecyclerView.ViewHolder{

        CardView cardView;
        TextView nombre;
        TextView universidad;
        TextView centro;


        public HolderVacante(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cvCurso);
            //bgImagen = (LinearLayout) itemView.findViewById(R.id.bgImagenLogin);

            nombre = (TextView) itemView.findViewById(R.id.txtNombre);
            universidad = (TextView) itemView.findViewById(R.id.txtUniversidad);
            centro = (TextView) itemView.findViewById(R.id.txtCentroInvestigacion);
            //informacion  = (TextView) itemView.findViewById(R.id.txtInformacion);
        }
    }

    public void ejecutarEliminar(String curso){
        Map<String, String> parametros = new HashMap<>();
        parametros.put("usuario",IDUSUARIO);
        parametros.put("clase",curso);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, URL, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String prueba = response.getString("resultado");
                    Toast.makeText(context,prueba, Toast.LENGTH_SHORT).show();
                    ((Activity)context).finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(,"Error en: "+ error,Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,context,volley);
    }
}

