package com.inf931.pise.Informacion;



import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inf931.pise.R;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilFragment extends Fragment {

    TextView txtNombre;
    TextView txtCarrera;
    TextView txtFechaNacimiento;
    TextView txtSexo;
    ImageView imgPerfil;

    String LIGA;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil, container, false);

        SharedPreferences prefs = getActivity().getBaseContext().getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);
        String idusuario = prefs.getString("idusuario","No Disponible");
        String nombre = prefs.getString("nombre","No Disponible");
        String paterno = prefs.getString("paterno","No Disponible");
        String materno = prefs.getString("materno","No Disponible");
        String sexo = prefs.getString("sexo", "No Disponible");
        String natalicio = prefs.getString("natalicio","No Disponible");
        String STATUS = prefs.getString("STATUS", "No Disponible");
        String email = prefs.getString("email", "No Disponible");
        String usuario = prefs.getString("usuario", "No Disponible");
        String pass = prefs.getString("pass", "No Disponible");
        String url_foto = prefs.getString("url_foto", "No Disponible");
        String carrera = prefs.getString("carrera", "No Disponible");

        LIGA = "http://travelopolis.ddns.net/pise/archivo/"+url_foto;

        txtNombre = (TextView) view.findViewById(R.id.txtNombreD);
        txtCarrera = (TextView) view.findViewById(R.id.txtCarerra);
        txtFechaNacimiento = (TextView) view.findViewById(R.id.txtFecha);
        txtSexo = (TextView) view.findViewById(R.id.txtSexo);
        imgPerfil = (ImageView) view.findViewById(R.id.imgPerfil);

        //Toast.makeText(getContext(), nombre+paterno+materno, Toast.LENGTH_SHORT).show();

        txtNombre.setText(nombre);
        txtCarrera.setText(carrera);
        txtFechaNacimiento.setText(natalicio);

        txtSexo.setText(sexo);
        CargaImagenes nuevaTarea = new CargaImagenes();
        nuevaTarea.execute(LIGA);

        return view;

    }

    private class CargaImagenes extends AsyncTask<String, Void, Bitmap> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Cargando Imagen");
            pDialog.setCancelable(true);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();

        }

        @Override
        protected Bitmap doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.i("doInBackground" , "Entra en doInBackground");
            String url = params[0];
            Bitmap imagen = descargarImagen(url);
            return imagen;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            imgPerfil.setImageBitmap(result);
            pDialog.dismiss();
        }

    }

    private Bitmap descargarImagen (String imageHttpAddress){
        URL imageUrl = null;
        Bitmap imagen = null;
        try{
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            imagen = BitmapFactory.decodeStream(conn.getInputStream());
        }catch(IOException ex){
            ex.printStackTrace();
        }

        return imagen;
    }
}
