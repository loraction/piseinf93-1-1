package com.inf931.pise.Informacion;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.inf931.pise.R;
import com.inf931.pise.VolleyRP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactoFragment extends Fragment {

    EditText txtTelefonoFijo;
    EditText txtTelefonoMovil;
    EditText txtCorreo;
    EditText txtCorreoAlterno;

    Button btnActualizar;
    private VolleyRP volley;
    private RequestQueue mRequest;

    String CONTACTO = "http://travelopolis.ddns.net/pise/login_GETCONTACTO.php?id=";
    String ACTUALIZAR_CONTACTO = "http://travelopolis.ddns.net/pise/Login_INSERT.php";
    String correoPiv;
    String correoAltPiv;
    String telefonoFijoPiv;
    String telefonoMovilPiv;
    String ID_USUARIO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacto, container, false);

        SharedPreferences prefs = getActivity().getBaseContext().getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);
        String idusuario = prefs.getString("idusuario","No Disponible");
        ID_USUARIO = idusuario;
        String nombre = prefs.getString("nombre","No Disponible");
        String paterno = prefs.getString("paterno","No Disponible");
        String materno = prefs.getString("materno","No Disponible");
        String sexo = prefs.getString("sexo", "No Disponible");
        String natalicio = prefs.getString("natalicio","No Disponible");
        String STATUS = prefs.getString("STATUS", "No Disponible");
        String email = prefs.getString("email", "No Disponible");
        String usuario = prefs.getString("usuario", "No Disponible");
        String pass = prefs.getString("pass", "No Disponible");
        String url_foto = prefs.getString("url_foto", "No Disponible");
        String carrera = prefs.getString("carrera", "No Disponible");

        volley = VolleyRP.getInstance(getContext());
        mRequest = volley.getRequestQueue();

        txtTelefonoFijo = (EditText) view.findViewById(R.id.txtTelefonoFijo);
        txtTelefonoMovil = (EditText) view.findViewById(R.id.txtTelefonoMovil);
        txtCorreo = (EditText) view.findViewById(R.id.txtCorreo);
        txtCorreoAlterno = (EditText) view.findViewById(R.id.txtCorreoAlterno);
        btnActualizar = (Button) view.findViewById(R.id.btnActualizar);

        solicitudJSON(CONTACTO+idusuario);

        //Toast.makeText(getActivity(), correoPiv+ correoAltPiv+telefonoFijoPiv+telefonoMovilPiv, Toast.LENGTH_SHORT).show();

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtTelefonoFijo.getText().toString().equals(telefonoFijoPiv)&&
                        txtTelefonoMovil.getText().toString().equals(telefonoMovilPiv)&&
                        txtCorreo.getText().toString().equals(correoPiv)&&
                        txtCorreoAlterno.getText().toString().equals(correoAltPiv))
                {
                    Fact(2);
                }else{
                    actualizarContacto();
                    Fact(3);
                }
            }
        });
        return view;
    }

    public void solicitudJSON(String URL){
        JsonObjectRequest solicitud = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject datos) {
                verificarContacto(datos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Ocurrio un Error"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,getContext(),volley);
    }

    public void verificarContacto(JSONObject datos){
        //Controllar el JSON
        //Toast.makeText(this,"Los Datos son:"+datos.toString(),Toast.LENGTH_SHORT).show();
        try{
            JSONObject JsonDatos = new JSONObject(datos.getString("datos"));
            String telefonoFijo= JsonDatos.getString("telefono");
            String telefonoMovil = JsonDatos.getString("celular");
            String correo = JsonDatos.getString("email");
            String correoAlt = JsonDatos.getString("email_alt");
            if(telefonoFijo.equals("null")&&telefonoMovil.equals("null")&&correo.equals("null")&&correoAlt.equals("null")){
                Fact(1);
                txtTelefonoFijo.setText(telefonoFijo);
                txtTelefonoMovil.setText(telefonoMovil);
                txtCorreo.setText(correo);
                txtCorreoAlterno.setText(correoAlt);

                correoPiv = correo;
                correoAltPiv = correoAlt;
                telefonoFijoPiv = telefonoFijo;
                telefonoMovilPiv = telefonoMovil;

            }
            else{
                txtTelefonoFijo.setText(telefonoFijo);
                txtTelefonoMovil.setText(telefonoMovil);
                txtCorreo.setText(correo);
                txtCorreoAlterno.setText(correoAlt);

                correoPiv = correo;
                correoAltPiv = correoAlt;
                telefonoFijoPiv = telefonoFijo;
                telefonoMovilPiv = telefonoMovil;
            }

        }catch (JSONException e){}
    }

    public void Fact(int seleccion){
        //se prepara la alerta creando nueva instancia
        AlertDialog.Builder alertbox = new AlertDialog.Builder(getContext());
        //seleccionamos la cadena a mostrar
        if(seleccion == 1){
            alertbox.setMessage("Agrega tu Informacion de Contacto");
        }
        if(seleccion == 2){
            alertbox.setMessage("No se Realizaron Cambios");
        }
        if(seleccion == 3){
            alertbox.setMessage("Contacto Actualizado");
        }
        alertbox.show();
    }

    public void actualizarContacto(){
        Map<String, String> parametros = new HashMap<>();
        parametros.put("telefonoFijo",txtTelefonoFijo.getText().toString());
        parametros.put("telefonoMovil",txtTelefonoMovil.getText().toString());
        parametros.put("correo",txtCorreo.getText().toString());
        parametros.put("correoAlterno",txtCorreoAlterno.getText().toString());
        parametros.put("id_alumno",ID_USUARIO);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, ACTUALIZAR_CONTACTO, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(getActivity(), response.getString("resultado"),Toast.LENGTH_SHORT).show();
                    getActivity().finishAndRemoveTask();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Error en: "+ error,Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(solicitud);

    }

}
