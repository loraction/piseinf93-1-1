package com.inf931.pise.Actividad;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.inf931.pise.R;
import com.inf931.pise.VolleyRP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */

public class PosgradoFragment extends Fragment {

    TextView txtNombre;
    EditText txtNombrePosgrado;
    EditText txtNombreEscuela;
    RadioButton rdbSi;
    RadioButton rdbNo;
    Button btnActualizar;

    String nomPosPiv;
    String nomEscPiv;
    String seleccionPiv;


    private VolleyRP volley;
    private RequestQueue mRequest;

    String NOMBRE;
    String ID_USUARIO;
    String posInt = "0";

    String URL = "http://travelopolis.ddns.net/pise/Actividad_GETPOS.php?usuario=";
    String ACTUALIZAR_CONTACTO = "http://travelopolis.ddns.net/pise/Actividad_UPDPOS.php";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_posgrado, container, false);

        SharedPreferences prefs = getActivity().getBaseContext().getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);
        String idusuario = prefs.getString("idusuario","No Disponible");
        ID_USUARIO = idusuario;
        String nombre = prefs.getString("nombre","No Disponible");
        NOMBRE = nombre;

        txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        txtNombrePosgrado = (EditText) view.findViewById(R.id.txtNombrePosgrado);
        txtNombreEscuela = (EditText) view.findViewById(R.id.txtEscuela);

        rdbSi = (RadioButton) view.findViewById(R.id.PosSi);
        rdbNo = (RadioButton) view.findViewById(R.id.PosNo);

        btnActualizar = (Button) view.findViewById(R.id.btnActualizar);

        volley = VolleyRP.getInstance(getActivity());
        mRequest = volley.getRequestQueue();

        solicitudJSON(URL+idusuario);

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(rdbSi.isChecked()==false&&rdbNo.isChecked()==false){
                     posInt = "0";
                }
                if(rdbSi.isChecked()==true){
                    posInt = "1";
                }
                if(rdbNo.isChecked()==true){
                    posInt = "2";
                }
                if(txtNombrePosgrado.getText().toString().equals(nomPosPiv)&&
                        txtNombreEscuela.getText().toString().equals(nomEscPiv)&&posInt.equals(seleccionPiv))
                {
                    Fact(2);
                }else{
                    actualizarContacto(posInt);
                    Fact(3);
                }
            }
        });
        return view;
    }

    public void solicitudJSON(String URL){
        JsonObjectRequest solicitud = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject datos) {
                verificarContacto(datos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Ocurrio un Error"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,getContext(),volley);
    }

    public void verificarContacto(JSONObject datos){
        //Controllar el JSON
        //Toast.makeText(this,"Los Datos son:"+datos.toString(),Toast.LENGTH_SHORT).show();
        try{
            JSONObject JsonDatos = new JSONObject(datos.getString("datos"));
            String nombre= JsonDatos.getString("nombre");
            String escuela = JsonDatos.getString("escuela");
            String deseo = JsonDatos.getString("deseo");
            if(deseo.matches("0")){
                Fact(1);
                txtNombre.setText(NOMBRE);
                txtNombrePosgrado.setText(nombre);
                txtNombreEscuela.setText(escuela);

                nomPosPiv = nombre;
                nomEscPiv = escuela;
                seleccionPiv = "0";
            }
            else{
                txtNombre.setText(NOMBRE);
                txtNombrePosgrado.setText(nombre);
                txtNombreEscuela.setText(escuela);

                nomPosPiv = nombre;
                nomEscPiv = escuela;
                if (deseo.matches("1")){
                    rdbSi.setChecked(true);
                    seleccionPiv = "1";
                }
                else {
                    rdbNo.setChecked(true);
                    seleccionPiv = "2";
                }
            }
        }catch (JSONException e){}
    }

    public void Fact(int seleccion){
        //se prepara la alerta creando nueva instancia
        AlertDialog.Builder alertbox = new AlertDialog.Builder(getContext());
        //seleccionamos la cadena a mostrar
        if(seleccion == 1){
            alertbox.setMessage("Contesta la Encuesta de Posgrado");
        }
        if(seleccion == 2){
            alertbox.setMessage("No se Realizaron Cambios");
        }
        if(seleccion == 3){
            alertbox.setMessage("Posgrado Actualizado");
        }
        alertbox.show();
    }

    public void actualizarContacto(String status){
        Map<String, String> parametros = new HashMap<>();
        parametros.put("nombre",txtNombrePosgrado.getText().toString());
        parametros.put("escuela",txtNombreEscuela.getText().toString());
        parametros.put("deseo",status);
        parametros.put("id_alumno",ID_USUARIO);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, ACTUALIZAR_CONTACTO, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(getActivity(), response.getString("resultado"), Toast.LENGTH_SHORT).show();
                    getActivity().finishAndRemoveTask();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Error en: "+ error, Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(solicitud);

    }


}
