package com.inf931.pise.Actividad;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.inf931.pise.R;
import com.inf931.pise.VolleyRP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */

public class LaboralFragment extends Fragment {

    TextView txtNombre;
    EditText txtPuesto;
    TextView txtEmpleoActual;
    EditText txtEmpresa;
    EditText txtFecha;
    Button btnFecha;
    Button btnActualizar;
    private VolleyRP volley;
    private RequestQueue mRequest;

    String AREA;
    String nomPPiv;
    String EmpresaPiv;
    String FechaPiv;

    String ID_USUARIO;

    private int dia, mes, ano;
    private int dia2, mes2, ano2;
    private String dia3, mes3, ano3;

    String URL= "http://travelopolis.ddns.net/pise/Empleos_GETEMPRESAS.php?usuario=";
    String URL2= "http://travelopolis.ddns.net/pise/Empleos_GETULTIMO.php?usuario=";
    String AGREGAR_EMPLEO = "http://travelopolis.ddns.net/pise/Empleos_INSERT.php";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_laboral, container, false);
        SharedPreferences prefs = getActivity().getBaseContext().getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);
        String idusuario = prefs.getString("idusuario","No Disponible");
        ID_USUARIO = idusuario;
        String nombre = prefs.getString("nombre","No Disponible");
        String carrera = prefs.getString("carrera", "No Disponible");

        if(carrera.matches("INFORMATICA")){
            AREA = "1";
        }



        txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        txtPuesto = (EditText)  view.findViewById(R.id.txtPuesto);
        txtFecha = (EditText) view.findViewById(R.id.txtFechaEntrada);
        txtEmpleoActual = (TextView) view.findViewById(R.id.txtUltimoEmpleo);
        txtEmpresa = (EditText) view.findViewById(R.id.txtEmpresa);
        btnActualizar = (Button) view.findViewById(R.id.btnActualizar);

        btnFecha = (Button) view.findViewById(R.id.btnFecha);

        txtNombre.setText(nombre);
        txtFecha.setEnabled(false);

        volley = VolleyRP.getInstance(getActivity());
        mRequest = volley.getRequestQueue();

        solicitudJSON(URL+idusuario);
        solicitudJSON2(URL2+idusuario);

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtPuesto.getText().toString().equals(nomPPiv)&&
                        txtFecha.getText().toString().equals(FechaPiv)&&txtEmpresa.getText().toString().equals(EmpresaPiv))
                {
                    Toast.makeText(getContext(),"Sin Cambios", Toast.LENGTH_SHORT).show();
                }
                else{
                    nuevoEmpleo();
                }
            }
        });

        btnFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                dia = c.get(Calendar.DAY_OF_MONTH);
                mes = c.get(Calendar.MONTH);
                ano = c.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        if(monthOfYear<9){
                            txtFecha.setText(year+"-"+"0"+(monthOfYear+1)+"-"+dayOfMonth);
                        }else{
                            txtFecha.setText(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
                        }
                        ano2=year;
                        mes2= monthOfYear+1;
                        dia2=dayOfMonth;
                    }
                }
                        ,ano, mes, dia);
                datePickerDialog.show();
            }
        });
        return view;
    }

    public void solicitudJSON(String URL){
        JsonObjectRequest solicitud = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject datos) {
                verificarContacto(datos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Ocurrio un Error"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,getContext(),volley);
    }

    public void verificarContacto(JSONObject datos){
        //Controllar el JSON
        //Toast.makeText(this,"Los Datos son:"+datos.toString(),Toast.LENGTH_SHORT).show();
        try{
            JSONObject JsonDatos = new JSONObject(datos.getString("datos"));
            String nombre= JsonDatos.getString("nombre");
            String nombreEmpresa = JsonDatos.getString("nombreEmpresa");
            String inicio = JsonDatos.getString("inicio");

                txtPuesto.setText(nombre);
                txtEmpresa.setText(nombreEmpresa);
                txtFecha.setText(inicio);

                nomPPiv = nombre;
                EmpresaPiv = nombreEmpresa;
                FechaPiv = inicio;

        }catch (JSONException e){}
    }

    public void solicitudJSON2(String URL){
        JsonObjectRequest solicitud = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject datos) {
                verificarContacto2(datos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Ocurrio un Error"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,getContext(),volley);
    }

    public void verificarContacto2(JSONObject datos){
        try{
            JSONObject JsonDatos = new JSONObject(datos.getString("datos"));
            String nombre= JsonDatos.getString("nombre");
            String nombreEmpresa = JsonDatos.getString("nombreEmpresa");
            String inicio = JsonDatos.getString("inicio");

            txtEmpleoActual.setText("Trabajo: " + nombre + " Empresa: " + nombreEmpresa + " Fecha de Inicio: " + inicio);

        }catch (JSONException e){}
    }

    public void nuevoEmpleo(){
        Map<String, String> parametros = new HashMap<>();
        parametros.put("nombre",txtPuesto.getText().toString());
        parametros.put("empresa",txtEmpresa.getText().toString());
        parametros.put("id_area",AREA);
        parametros.put("inicio",txtFecha.getText().toString());
        parametros.put("id_alumno",ID_USUARIO);
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, AGREGAR_EMPLEO, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(getActivity(), response.getString("resultado"), Toast.LENGTH_SHORT).show();
                    getActivity().finishAndRemoveTask();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Error en: "+ error, Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(solicitud);

    }
}
