package com.inf931.pise.Vacantes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inf931.pise.R;

import java.util.List;

public class VacantesAdapter extends RecyclerView.Adapter<VacantesAdapter.HolderVacante> {

    private List<VacanteConstructor> atributosList;
    private Context context;

    public VacantesAdapter(List<VacanteConstructor> atributosList, Context context){
        this.atributosList = atributosList;
        this.context = context;
    }

    @Override
    public VacantesAdapter.HolderVacante onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_vacante,parent,false);
        return new VacantesAdapter.HolderVacante(v);
    }

    @Override
    public void onBindViewHolder(final VacantesAdapter.HolderVacante holder, final int position) {
        //holder.id.setText(atributosList.get(position).getId());

        String id = atributosList.get(position).getId();
        String nombre = atributosList.get(position).getNombre();
        String descripcion = atributosList.get(position).getDescripcion();
        String empresa = atributosList.get(position).getEmpresa();
        String inicio = atributosList.get(position).getInicio();
        String fin = atributosList.get(position).getFin();


        holder.nombre.setText(nombre);
        holder.empresa.setText(empresa);


        //holder.informacion.setText(atributosList.get(position).getInformacion());

        /*if(atributosList.get(position).getMensaje().equals("null")){
            holder.hora.setVisibility(View.GONE);
            holder.mensaje.setText("Enviale un mensaje!");
        }else{
            holder.hora.setVisibility(View.VISIBLE);

            if(atributosList.get(position).getType_mensaje().equals("1")){
                holder.mensaje.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
            }else{
                holder.mensaje.setTextColor(ContextCompat.getColor(context, R.color.colorBlue));
            }

        }*/

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Vacante.class);
                i.putExtra("id",atributosList.get(position).getId());
                i.putExtra("descripcion",atributosList.get(position).getDescripcion());
                i.putExtra("empresa",atributosList.get(position).getEmpresa());
                i.putExtra("inicio",atributosList.get(position).getInicio());
                i.putExtra("fin",atributosList.get(position).getFin());
                context.startActivity(i);
                Toast.makeText(context, "Cargando Vacante", Toast.LENGTH_SHORT).show();
            }
        });

        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                /*new AlertDialog.Builder(context).
                        setMessage("¿Estas seguro que quieres eliminar a este amigo?").
                        setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(context, "Se elimino el amigo correctamente", Toast.LENGTH_SHORT).show();
                            }
                        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Toast.makeText(context, "Cancelando solicitud de eliminacion", Toast.LENGTH_SHORT).show();
                    }
                }).show();*/
                Toast.makeText(context, "Seleecion Larga", Toast.LENGTH_SHORT).show();
                return true;
            }
        });



    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }

    static class HolderVacante extends RecyclerView.ViewHolder{

        CardView cardView;
        //TextView id;
        TextView nombre;
        TextView empresa;
        TextView fechaInicio;
        TextView fechaFin;
        //TextView informacion;
        LinearLayout bgImagen;

        public HolderVacante(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cvInfoVacante);
            //bgImagen = (LinearLayout) itemView.findViewById(R.id.bgImagenLogin);

            nombre = (TextView) itemView.findViewById(R.id.txtNombre);
            empresa = (TextView) itemView.findViewById(R.id.txtEmpresa);
            //informacion  = (TextView) itemView.findViewById(R.id.txtInformacion);
        }
    }
}
