package com.inf931.pise.Vacantes;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.inf931.pise.R;

public class Vacante extends AppCompatActivity {

    TextView lblPuesto;
    TextView lblRequisitos;
    TextView lblEmpresa;

    String PUESTO;
    String EMPRESA;
    String REQUISITOS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacante);

        lblPuesto = (TextView) findViewById(R.id.lblPuesto);
        lblEmpresa = (TextView) findViewById(R.id.lblEmpresa);
        lblRequisitos = (TextView) findViewById(R.id.lblRequisitos);

        Intent extras = getIntent();
        Bundle bundle = extras.getExtras();
        if(bundle!=null){
            PUESTO=bundle.getString("id");
            EMPRESA=bundle.getString("empresa");
            REQUISITOS=bundle.getString("descripcion");
        }

        lblPuesto.setText(PUESTO);
        lblEmpresa.setText(EMPRESA);
        lblRequisitos.setText(REQUISITOS);

    }
}
