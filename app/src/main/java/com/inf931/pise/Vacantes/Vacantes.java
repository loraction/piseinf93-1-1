package com.inf931.pise.Vacantes;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.inf931.pise.R;
import com.inf931.pise.VolleyRP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Vacantes extends AppCompatActivity {

    private List<VacanteConstructor> atributosList;
    private VolleyRP volley;
    private RequestQueue mRequest;
    private RecyclerView rv;
    private VacantesAdapter adapter;
    Toolbar toolbar;

    String CARRERA;
    String IDCARRERA;
    String URL = "http://travelopolis.ddns.net/pise/Vacantes_GETALL.php?area=";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacantes);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Vacantes");
        setSupportActionBar(toolbar);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        SharedPreferences prefs = this.getBaseContext().getSharedPreferences("shared_login_data", Context.MODE_PRIVATE);
        String idusuario = prefs.getString("idusuario","No Disponible");
        String nombre = prefs.getString("nombre","No Disponible");
        String paterno = prefs.getString("paterno","No Disponible");
        String materno = prefs.getString("materno","No Disponible");
        String sexo = prefs.getString("sexo", "No Disponible");
        String natalicio = prefs.getString("natalicio","No Disponible");
        String STATUS = prefs.getString("STATUS", "No Disponible");
        String email = prefs.getString("email", "No Disponible");
        String usuario = prefs.getString("usuario", "No Disponible");
        String pass = prefs.getString("pass", "No Disponible");
        String url_foto = prefs.getString("url_foto", "No Disponible");
        String carrera = prefs.getString("carrera", "No Disponible");
        CARRERA = carrera;

        if(CARRERA.matches("INFORMATICA")){
            IDCARRERA = "1";
        }
        atributosList = new ArrayList<>();

        URL = "http://travelopolis.ddns.net/pise/Vacantes_GETALL.php?area="+IDCARRERA;

        rv =(RecyclerView) findViewById(R.id.rvVacantes);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);

        adapter = new VacantesAdapter(atributosList,this);
        rv.setAdapter(adapter);

        solicitudJSON();
    }

    public void solicitudJSON(){
        JsonObjectRequest solicitud = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject datos) {
                try {
                    String TodoslosDatos = datos.getString("resultado");
                    JSONArray jsonArray = new JSONArray(TodoslosDatos);
                    for (int i = 0; i<jsonArray.length();i++){
                        JSONObject js = jsonArray.getJSONObject(i);
                        agregarViaje(js.getString("id"),js.getString("nombre"),js.getString("descripcion"),
                                js.getString("inicio"),js.getString("fin"),js.getString("nombreEmpresa"));
                    }

                } catch (JSONException e) {
                    Toast.makeText(Vacantes.this,"Ocurrio un Error"+e, Toast.LENGTH_SHORT).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Vacantes.this,"Ocurrio un Error"+error, Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);
    }

    public void agregarViaje(String id, String nombre, String descripcion, String inicio, String fin, String empresa){

        Date fechaActual = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        String fechaSistema=formateador.format(fechaActual);


        int x = compararFecha(fechaSistema, fin);

        if(x==1){

            VacanteConstructor vacante = new VacanteConstructor();
            vacante.setId(id);
            vacante.setNombre(nombre);
            vacante.setDescripcion(descripcion);
            vacante.setEmpresa(empresa);
            vacante.setInicio(inicio);
            vacante.setFin(fin);
            atributosList.add(vacante);
            adapter.notifyDataSetChanged();
        }

    }

    public int compararFecha(String fechaActual, String fechaComparar){
    int x = 0;
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date fechaDate1 = formateador.parse(fechaComparar);
            Date fechaDate2 = formateador.parse(fechaActual);

            if ( fechaDate1.before(fechaDate2) ){
                x=1;
            }else{
                if ( fechaDate2.before(fechaDate1) ){
                    x=0;
                }else{
                    x=1;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return x;
    }
}
