package com.inf931.pise.Global;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.inf931.pise.Actividad.Actividad;
import com.inf931.pise.Cursos.Cursos;
import com.inf931.pise.Informacion.Perfil;
import com.inf931.pise.R;
import com.inf931.pise.Vacantes.Vacantes;

public class Inicio extends AppCompatActivity {

    ImageButton btnPerfil;
    ImageButton btnActividad;
    ImageButton btnVacante;
    ImageButton btnCursos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        btnPerfil = (ImageButton) findViewById(R.id.imgperfil);
        btnActividad = (ImageButton) findViewById(R.id.imgActividadActual);
        btnVacante = (ImageButton) findViewById(R.id.imgVacantes);
        btnCursos = (ImageButton) findViewById(R.id.imgCursos);

        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Perfil.class);
                startActivity(i);
            }
        });

        btnVacante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Vacantes.class);
                startActivity(i);
            }
        });

        btnActividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Actividad.class);
                startActivity(i);
            }
        });

        btnCursos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Cursos.class);
                startActivity(i);
            }
        });



    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            eleccion();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void eleccion(){
        //se prepara la alerta creando nueva instancia
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        //seleccionamos la cadena a mostrar
        alertbox.setMessage("Deseas Salir");
        //elegimos un positivo SI y creamos un Listener
        alertbox.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            //Funcion llamada cuando se pulsa el boton Si
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            public void onClick(DialogInterface arg0, int arg1) {
                finishAndRemoveTask();
                Toast.makeText(Inicio.this,"Cerrando Sesion",Toast.LENGTH_SHORT).show();
            }
        });

        //elegimos un positivo NO y creamos un Listener
        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            //Funcion llamada cuando se pulsa el boton No
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(Inicio.this,"Sesion Activa",Toast.LENGTH_SHORT).show();
            }
        });

        //mostramos el alertbox
        alertbox.show();
    }
}
