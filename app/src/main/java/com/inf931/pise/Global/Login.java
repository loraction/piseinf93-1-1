package com.inf931.pise.Global;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inf931.pise.R;
import com.inf931.pise.VolleyRP;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {

    EditText txtUsuario;
    EditText txtContraseña;
    Button btnIngresar;
    Button btnFact;


    private VolleyRP volley;
    private RequestQueue mRequest;

    private String USER = "";
    private String ID_USER = "";
    private String TIPOUSER = "";
    private String NOMBRE_USER = "";
    private String PASSWORD = "";

    private static final String ip = "http://travelopolis.ddns.net/pise/login_GETINFO.php?usuario=";
    //private static final String IP_TOKEN = "http://travelopolis.ddns.net/profapp/token_INSERTandUPDATE.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();


        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtContraseña = (EditText) findViewById(R.id.txtContrasena);
        btnIngresar = (Button) findViewById(R.id.btnIniciarL);
        btnFact = (Button) findViewById(R.id.btnFact);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contraseñaEncryp = new String(Hex.encodeHex(DigestUtils.sha1(txtContraseña.getText().toString())));
               // Toast.makeText(Login.this,txtUsuario.getText().toString(),Toast.LENGTH_SHORT).show();
                //.makeText(Login.this,txtContraseña.getText().toString(),Toast.LENGTH_SHORT).show();
                verificarLogin(txtUsuario.getText().toString(), contraseñaEncryp);
            }
        });

        /*btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),SeleccionRegistro.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });*/

        btnFact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fact();
            }
        });
    }

    public void verificarLogin(String usuario, String contraseña){
        USER = usuario;
        PASSWORD = contraseña;
       // Toast.makeText(this,"El usuarios es:"+usuario+" y la contraseña es: "+contraseña,Toast.LENGTH_SHORT).show();
        solicitudJSON(ip+usuario);

    }

    public void solicitudJSON(String URL){
        JsonObjectRequest solicitud = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject datos) {
                verificarPassword(datos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Login.this,"Ocurrio un Error"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);
    }

    public void verificarPassword(JSONObject datos){
        //Controllar el JSON
        //Toast.makeText(this,"Los Datos son:"+datos.toString(),Toast.LENGTH_SHORT).show();
        try{
            String estado = datos.getString("resultado");
            if(estado.equals("CC")){
                //Toast.makeText(this, "Contraseña y Usuario Correcto",Toast.LENGTH_SHORT).show();
                JSONObject JsonDatos = new JSONObject(datos.getString("datos"));
                String idusuario = JsonDatos.getString("id");
                ID_USER = idusuario;
                String nombre = JsonDatos.getString("nombre");
                NOMBRE_USER = nombre;
                String paterno = JsonDatos.getString("paterno");
                String materno = JsonDatos.getString("materno");
                String natalicio = JsonDatos.getString("natalicio");
                String sexo = JsonDatos.getString("sexo");
                String STATUS = JsonDatos.getString("STATUS");
                TIPOUSER = STATUS;
                String usuario = JsonDatos.getString("usuario");
                String pass = JsonDatos.getString("PASSWORD");
                String email = JsonDatos.getString("email");
                String url_foto = JsonDatos.getString("url_foto");
                String carrera = JsonDatos.getString("carrera");
                if(usuario.equals(USER) && pass.equals(PASSWORD)){
                    Toast.makeText(this, "LOGIN CORRECTO", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(this, viajero + ":"+JsonDatos.getString("viajeroID"), Toast.LENGTH_SHORT).show();
                    //ALMACEN DE DATOS DE LOGIN
                    SharedPreferences prefs = getSharedPreferences("shared_login_data",   Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("idusuario", idusuario);
                    editor.putString("nombre", nombre);
                    editor.putString("paterno", paterno);
                    editor.putString("materno", materno);
                    editor.putString("natalicio", natalicio);
                    editor.putString("sexo", sexo);
                    editor.putString("STATUS", STATUS);
                    editor.putString("email", email);
                    editor.putString("usuario", usuario);
                    editor.putString("pass", pass);
                    editor.putString("url_foto", url_foto);
                    editor.putString("carrera", carrera);
                    editor.commit();

                    if(TIPOUSER.matches("1")){
                        Intent i = new Intent(getApplicationContext(), Inicio.class);
                        i.putExtra("id_emisor",ID_USER);
                        i.putExtra("nombre_emisor",NOMBRE_USER);
                        startActivity(i);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }else{
                        Toast.makeText(Login.this,"Tipo de Usuario No Valido", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this, "Contraseña Incorrecta", Toast.LENGTH_SHORT).show();
                }

            }else{
                Toast.makeText(this, estado, Toast.LENGTH_SHORT).show();
            }

        }catch (JSONException e){}
    }

    public void Fact(){
        //se prepara la alerta creando nueva instancia
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        //seleccionamos la cadena a mostrar
        alertbox.setMessage("Dudas o Problemas de Cuenta: Envia Un Mensaje a: crescendo@gmail.com");
        //elegimos un positivo SI y creamos un Listener
        //mostramos el alertbox
        alertbox.show();
    }

}
